/**
 * maven-html5manifest-plugin,
 * maven plugin for generating HTML5 cache manifest files
 * Copyright (c) 2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.evolvis.maven.plugin.html5manifest;

import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

/**
 * @phase generated-resources
 * @goal generate-manifest
 * @threadSafe true
 */
public class Html5ManifestMojo extends AbstractMojo {

	/**
	 * Path to the manifest file that should be generated, default is '${basedir}/src/main/webapp/offline.appcache' for
	 * fast development with 'jetty:run', normally you would replace it.
	 * 
	 * @parameter expression="${outputManifestPath}" default-value="${basedir}/src/main/webapp/offline.appcache"
	 */
	private String outputManifestPath;

	/**
	 * A version number for informing clients that something changed and all resources should be refresh, default is '${project.version}'.
	 * 
	 * @parameter expression="${manifestVersion}" default-value="${project.version}"
	 */
	private String manifestVersion;

	/**
	 * A set of {@link FileSet} filters of real resources like js files that should be cached by the client. Default is:
	 * <ul>
	 * <li>directory = "src/main/webapp"
	 * <li>includes = ["*.css", "**\/*.js", "**\/*.gif", "**\/*.png", "**\/*.jpg"]
	 * </ul>
	 * @parameter
	 */
	private List<FileSet> realResources = new ArrayList<FileSet>();
	
	/**
	 * A set of strings of all abstract entries like dynamic html files that will be generated at runtime and should be cached by the client.
	 * 
	 * @parameter
	 */
	private List<String> abstractResources = new ArrayList<String>();
	
	/**
	 * A set of strings defining network resources that shouldn't be cached.
	 * 
	 * @parameter
	 */
	private List<String> networkResources = new ArrayList<String>();

	/**
	 * The fallback expression
	 * 
	 * @parameter
	 */
	private String fallback;

	public String getOutputManifestPath() {
		return outputManifestPath;
	}

	public void setOutputManifestPath(String outputManifestPath) {
		this.outputManifestPath = outputManifestPath;
	}

	public String getManifestVersion() {
		return manifestVersion;
	}

	public void setManifestVersion(String manifestVersion) {
		this.manifestVersion = manifestVersion;
	}

	public List<FileSet> getRealResources() {
		return realResources;
	}

	public void setRealResources(List<FileSet> realResources) {
		this.realResources = realResources;
	}

	public List<String> getAbstractResources() {
		return abstractResources;
	}

	public void setAbstractResources(List<String> abstractResources) {
		this.abstractResources = abstractResources;
	}

	public List<String> getNetworkResources() {
		return networkResources;
	}

	public void setNetworkResources(List<String> networkResources) {
		this.networkResources = networkResources;
	}

	public String getFallback() {
		return fallback;
	}

	public void setFallback(String fallback) {
		this.fallback = fallback;
	}

	public void execute() throws MojoExecutionException {
		
		final FileSetManager fileSetManager = new FileSetManager(getLog());
		
		if (realResources.isEmpty()) {
			final FileSet webappSet = new FileSet();
			webappSet.setDirectory("src/main/webapp");
			webappSet.addInclude("**/*.css");
			webappSet.addInclude("**/*.js");
			webappSet.addInclude("**/*.gif");
			webappSet.addInclude("**/*.png");
			webappSet.addInclude("**/*.jpg");
			realResources.add(webappSet);
		}
		
		final Set<String> realResourcesEntries = new TreeSet<String>();
		
		for (FileSet realResource : realResources) {
			for (String fileToInclude : fileSetManager.getIncludedFiles(realResource)) {
				realResourcesEntries.add(fileToInclude);
			}
		}
		
		final Set<String> abstractResourceEntries = new TreeSet<String>(abstractResources);
		
		final Set<String> networkResourceEntries = new TreeSet<String>(networkResources);
		
		try {
			FileUtils.touch(new File(outputManifestPath));
			final PrintWriter manifest = new PrintWriter(outputManifestPath, "UTF-8");

			manifest.println("CACHE MANIFEST");
			manifest.println();
			manifest.println("# version: " + this.manifestVersion);
			
			if (!realResourcesEntries.isEmpty()) {
				manifest.println();
				manifest.println("# real resources entries");
				for (String realResource : realResourcesEntries) {
					manifest.println(realResource);
				}
			}
			
			if (!abstractResourceEntries.isEmpty()) {
				manifest.println();
				manifest.println("# abstract resources entries");
				for (String abstractResource : abstractResourceEntries) {
					manifest.println(abstractResource);
				}
			}

			if (!networkResourceEntries.isEmpty()) {
				manifest.println();
				manifest.println("NETWORK:");
				for (String networkResource : networkResourceEntries) {
					manifest.println(networkResource);
				}
			}
			
			if(fallback != null && !fallback.isEmpty()) {
				manifest.println();
				manifest.println("FALLBACK:");
				manifest.println(fallback);
			}
			
			manifest.flush();
			manifest.close();
		}
		catch (IOException ex) {
			getLog().error("generating cache manifest failed with error: " + ex.getLocalizedMessage(), ex);
		}
	}
}