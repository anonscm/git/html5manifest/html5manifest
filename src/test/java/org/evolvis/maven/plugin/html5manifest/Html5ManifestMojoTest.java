/**
 * maven-html5manifest-plugin,
 * maven plugin for generating HTML5 cache manifest files
 * Copyright (c) 2012 tarent solutions GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License, version 3
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.evolvis.maven.plugin.html5manifest;

import static org.junit.Assert.*;
import static junitx.framework.FileAssert.*;

import java.io.File;
import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;

public class Html5ManifestMojoTest {

	private Html5ManifestMojo testMojo;
	
	@Before
	public void setUp() throws Exception {
		testMojo = new Html5ManifestMojo();
	}

	@Test
	public void testDefaultManifest() throws Exception {
		testMojo.setOutputManifestPath("target/generated-sources/test.default.appcache");
		testMojo.execute();		
		assertEquals(new File("src/test/resources/defaultTest.appcache"), new File("target/generated-sources/test.default.appcache"));
	}

	@Test
	public void testDefaultManifestWithAbstractsSection() throws Exception {
		testMojo.setOutputManifestPath("target/generated-sources/test.abstract.appcache");
		testMojo.setAbstractResources(Arrays.asList("http://foo.com"));
		testMojo.execute();		
		assertEquals(new File("src/test/resources/abstractTest.appcache"), new File("target/generated-sources/test.abstract.appcache"));
	}

	@Test
	public void testDefaultManifestWithNetworkSection() throws Exception {
		testMojo.setOutputManifestPath("target/generated-sources/test.network.appcache");
		testMojo.setNetworkResources(Arrays.asList("*"));
		testMojo.execute();		
		assertEquals(new File("src/test/resources/networkTest.appcache"), new File("target/generated-sources/test.network.appcache"));
	}

	@Test
	public void testDefaultManifestWithFallbackSection() throws Exception {
		testMojo.setOutputManifestPath("target/generated-sources/test.fallback.appcache");
		testMojo.setFallback("/index.html");
		testMojo.execute();		
		assertEquals(new File("src/test/resources/fallbackTest.appcache"), new File("target/generated-sources/test.fallback.appcache"));
	}

	@Test
	public void testFullManifestCreation() throws Exception {
		testMojo.setOutputManifestPath("target/generated-sources/test.full.appcache");
		testMojo.setAbstractResources(Arrays.asList("http://foo.com"));
		testMojo.setNetworkResources(Arrays.asList("*"));
		testMojo.setFallback("/index.html");
		testMojo.execute();		
		assertEquals(new File("src/test/resources/fullTest.appcache"), new File("target/generated-sources/test.full.appcache"));
	}
}
